public class AvionCarga extends Avion{
    //Atributos
    private String color;
    private double capacidad;

    public AvionCarga(){

    }

    public void despegar(){
        System.out.println("despegando..");
    }

    public void aterrizar(){
        System.out.println("aterrizar...");
    }

    public void cargar(){
        System.out.println("cargando...");
    }
}
